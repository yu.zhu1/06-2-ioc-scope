package com.twuc.webApp.yourTurn;

import org.assertj.core.error.ShouldHaveOnlyElementsOfType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import static org.junit.jupiter.api.Assertions.*;

public class SingletonIntegrationTest {


    private AnnotationConfigApplicationContext context;

    @BeforeEach
    public void createContext() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void should_return_the_same_when_interface() {
        InterfaceOneImpl result1 = context.getBean(InterfaceOneImpl.class);
        InterfaceOne result2 = context.getBean(InterfaceOne.class);
        assertSame(result1, result2);
    }

    @Test
    void should_return_the_same_when_extends() {
        ExtendsOneImpl result1 = context.getBean(ExtendsOneImpl.class);
        ExtendsOne result2 = context.getBean(ExtendsOne.class);
        assertSame(result1, result2);
    }

    @Test
    void should_return_the_same_when_extends_abstract_base_class() {
        AbstractBaseClass result1 = context.getBean(AbstractBaseClass.class);
        DerivedClass result2 = context.getBean(DerivedClass.class);
        assertSame(result1, result2);
    }

    /* 2.2.1 */
    @Test
    void should_return_different_beans() {
        SimplePrototypeScopeClass result1 = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass result2 = context.getBean(SimplePrototypeScopeClass.class);
        assertNotSame(result1, result2);
    }

    /* 2.2.2 */
    @Test
    void should_create_bean_when_new_the_context_for_singleton() {
        Logger logger = context.getBean(Logger.class);
        assertEquals(1, logger.getLogger().size());
    }

    @Test
    void should_create_been_when_get_Bean_for_Prototype() {
        Logger logger = context.getBean(Logger.class);
        assertEquals(1, logger.getLogger().size());
        SimplePrototypeScopeClass bean = context.getBean(SimplePrototypeScopeClass.class);
        assertEquals(2,logger.getLogger().size());
    }

    /* 2.2.3 */
    @Test
    void should_lazy_create_bean() {
        Logger logger = context.getBean(Logger.class);
        assertEquals(1, logger.getLogger().size());
        SingletonLazy lazy = context.getBean(SingletonLazy.class);
        assertEquals(2, logger.getLogger().size());
    }

    /* 2.2.4 */
    @Test
    void should_return() {
        PrototypeScopeDependsOnSingleton result1 = context.getBean(PrototypeScopeDependsOnSingleton.class);
        PrototypeScopeDependsOnSingleton result2 = context.getBean(PrototypeScopeDependsOnSingleton.class);
        assertNotSame(result1, result2);
        assertSame(result1.getSingletonDependent(), result2.getSingletonDependent());
    }

    /* 2.2.5 */
    @Test
    void should_return_different_beans_for_prototype() {
        SingletonDependsOnPrototypeProxy result1 = context.getBean(SingletonDependsOnPrototypeProxy.class);
        SingletonDependsOnPrototypeProxy result2 = context.getBean(SingletonDependsOnPrototypeProxy.class);
        assertSame(result1, result2);
        assertSame(result1.getPrototypeDependentWithProxy(), result2.getPrototypeDependentWithProxy());
    }



    /* 2.3 */
    @Test
    void should_return_new_beans_for_each_get_bean_from_singleton() {
        SingletonDependsOnPrototypeProxy result1 = context.getBean(SingletonDependsOnPrototypeProxy.class);
        SingletonDependsOnPrototypeProxy result2 = context.getBean(SingletonDependsOnPrototypeProxy.class);
//        int count1 = result1.getPrototypeDependentWithProxy().getCount();
//        int count2 = result2.getPrototypeDependentWithProxy().getCount();
        PrototypeDependentWithProxy outcome1 = result1.getPrototypeDependentWithProxy();
        PrototypeDependentWithProxy outcome2 = result2.getPrototypeDependentWithProxy();
        assertSame(outcome1, outcome2);
    }


}