package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrototypeScopeDependsOnSingleton {
    public SingletonDependent getSingletonDependent() {
        return singletonDependent;
    }

    public PrototypeScopeDependsOnSingleton(Logger logger, SingletonDependent singletonDependent) {
        this.logger = logger;
        this.singletonDependent = singletonDependent;
        this.logger.addLog("prototype");
    }

    public Logger getLogger() {
        return logger;
    }

    private Logger logger;
    private SingletonDependent singletonDependent;

}
