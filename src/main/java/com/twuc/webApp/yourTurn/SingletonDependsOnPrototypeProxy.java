package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxy {

    public SingletonDependsOnPrototypeProxy(PrototypeDependentWithProxy prototypeDependentWithProxy) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
    }
    @Autowired
    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        prototypeDependentWithProxy.getCount();
        return prototypeDependentWithProxy;
    }

    private PrototypeDependentWithProxy prototypeDependentWithProxy;
}
