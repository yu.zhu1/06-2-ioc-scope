package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependent {
    private Logger logger;

    public SingletonDependent(Logger logger) {
        this.logger = logger;
        this.logger.addLog("singleton");
    }
}
