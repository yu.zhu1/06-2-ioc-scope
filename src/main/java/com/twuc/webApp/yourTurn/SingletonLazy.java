package com.twuc.webApp.yourTurn;

import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public class SingletonLazy {
    private Logger logger;

    public  SingletonLazy(Logger logger) {
        this.logger = logger;
        this.logger.addLog("singleton");
    }
}
