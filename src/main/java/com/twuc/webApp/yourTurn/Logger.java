package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class Logger {

    private ArrayList<String> logger = new ArrayList<>();

    public void addLog(String log) {
        logger.add(log);
    }

    public ArrayList<String> getLogger() {
        return logger;
    }
}
