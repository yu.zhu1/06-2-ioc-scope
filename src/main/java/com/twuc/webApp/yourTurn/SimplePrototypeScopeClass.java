package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SimplePrototypeScopeClass {
    public SimplePrototypeScopeClass(Logger logger) {
        this.logger = logger;
        this.logger.addLog("when get beans");
    }

    public Logger getLogger() {
//        logger.addLog("when use method");
        return logger;
    }

    private Logger logger;

}
